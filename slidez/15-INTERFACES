
!!center
<blue>INTERFACES<end>
!!nocenter
      <purple>Interface Definition:<end>
      - Defines a <yellow>contract<end> for how a <yellow>component<end> or <yellow>svc<end> is <yellow>consumed<end>.
      - <yellow>Tested<end> in a build pipeline to validate <yellow>compliance<end>.
      - <yellow>Coupled<end> with a <yellow>service<end> or <yellow>component version<end>.
      - <yellow>Abstracts<end> complex implementation details.

    <cyan>█<end> Whenever some consumer <yellow>couples<end> to the <yellow>interface<end> of a <yellow>component<end> to
    <cyan>█<end> make use of its behaviour, a <yellow>contract is formed between them<end>. This
    <cyan>█<end> contract consists of <yellow>expectations of I/O data structures<end>,
    <cyan>█<end> <yellow>side effects<end>, and <yellow>performance<end> & concurrency characteristics.
    <cyan>██ Toby Clemson - Microservices Testing, 2014 <green>[11]

      <purple>Examples:<end>
      - GitHook to validate commit message before pushing code.
      - Swagger file defining paths, operations, and I/O of an API.
      - BDD <yellow>"given, when, then"<end> test of expected behavior.
      - <yellow>Sentinel<end> policy to enforce ACLs.


