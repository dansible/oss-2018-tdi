#!/bin/bash

# How the heck do you apply TDD to Infra as Code??
# Presentation for Open Source Summit 2018 Vancouver
# Daniel Pacrami (DevOps @ SAP Montreal-QC-Canada)
# @dansible
# https://gitlab.com/dansible/oss-2018-tdi
# https://dansible.gitlab.io

# set -x

WORK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
BIN_DIR="${WORK_DIR}/bin"
SLIDES_DIR="${WORK_DIR}/slidez"

source "${BIN_DIR}/slide.sh"
deck "${SLIDES_DIR}"

