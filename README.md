# Open Source Summit 2018 - TDI

This repo contains the presentation files for [How the Heck do you apply TDD to Infrastructure as Code??](https://ossna18.sched.com/event/FAOD/how-the-heck-do-you-apply-tdd-to-infra-as-code-daniel-pacrami-sap-canada) ([Open Source Summit 2018](https://events.linuxfoundation.org/events/open-source-summit-north-america-2018/) in Vancouver, BC, Canada).


Presentation is done using `slide.sh` script by @ryanuber:

- https://github.com/ryanuber/slide.sh



